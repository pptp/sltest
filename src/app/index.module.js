(function() {
  'use strict';

  angular
    .module('test', ['ngMessages', 'ngAria', 'ngRoute', 'ui.bootstrap', 'toastr']);

})();
