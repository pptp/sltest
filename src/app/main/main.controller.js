(function() {
  'use strict';

  angular
    .module('test')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($http, $timeout) {
    var vm = this;

    $http.get('countries.json')
        .then(function(res) {
          vm.countryList = res.data;
        });

    vm.search = '';
    vm.choosed = false;
    vm.showList = false;

    var getCountryByCode = function(code) {
      for (var i in vm.countryList) {
        if (vm.countryList.hasOwnProperty(i)) {
          if (vm.countryList[i]['cca3'] == code) {
            return vm.countryList[i];
          }
        }
      }
    }

    vm.doShowList = function() {
      vm.showList = true;
    }

    vm.doHideList = function() {
      $timeout(function() {
        vm.showList = false;
      }, 100);
    }

    vm.chooseCountry = function(country) {
      vm.choosed = country;
      vm.search = country.name.official;
    }

    vm.getChoosedBordered = function() {
      var bordered = [];
      var country;
      for (var i in vm.choosed.borders) {
        if (vm.choosed.borders.hasOwnProperty(i)) {
          country = getCountryByCode(vm.choosed.borders[i]);
          if (country) { // мало ли
            bordered.push(country);
          }
        }
      }

      return bordered;
    }

    vm.doSearch = function(item) {
      return (item.name.official.toLowerCase().indexOf(vm.search.toLowerCase()) != -1) || (item.capital.toLowerCase().indexOf(vm.search.toLowerCase()) != -1) || (item.callingCode.indexOf(vm.search) != -1);
    }
  }

  angular
    .module('test')
    .directive("outsideClick", outsideClick);

    function outsideClick($document) {
      return {
        link: function($scope, $element, $attributes) {
          var scopeExpression = $attributes.outsideClick,
            onDocumentClick = function(event) {
              var isChild = false;

              // we can use original jQuery to prevent this bycicle
              var searchEl = angular.element(event.target);
              while (searchEl.length && !isChild) {
                if (searchEl[0] == $element[0]) {
                  isChild = true;
                }
                searchEl = searchEl.parent();
              }
              if (!isChild) {
                $scope.$apply(scopeExpression);
              }
            };

          $document.on("click", onDocumentClick);

          $element.on('$destroy', function() {
            $document.off("click", onDocumentClick);
          });
        }
      }
    }

})();
